/*
  Copyright 2019 Stefan Waldschmidt
    
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

/**
 * @brief Tick-based scheduler.
 *
 * The user has full control over all "atomic operations" and when they are schduled.
 * 
 * @author Stefan Waldschmidt
 * @copyright Apache License, Version 2.0
 * 
 * https://gitlab.com/swal-source-flakes/ticked-scheduler
 */

#ifndef TICKED_SCHEDULER_H_
#define TICKED_SCHEDULER_H_


#if defined(__cplusplus)
extern "C" {
#endif

/* forward declaration, 
 * needed because the scheduler contains a list on functions, which takes the scheduler itself as an argument
 */
struct ts_scheduler;

/**
 * All operations the scheduler can perfom need to be implemented in functions with this signature 
 * 
 * @param ts_scheduler instance of the scheduler calling this function
 */
typedef void ts_operation_function(struct ts_scheduler *ts, void *arguments);

/**
 * This is what the schduler's "program" consists of: function pointers and arguments
 */
struct ts_operation {
  ts_operation_function *function;
  void *arguments;
};

/**
 * @brief Local context on an execution-thread
 *  
 * Every execution thread can have a user-provided context (think of a set of registers).
 * The scheduler does not cate about this context. 
 * 
 * The context is passed (by reference) into every user-defind operation. The operation
 * can read or write the context.
 */
typedef void *ts_context;

/**
 * Instance of a scheduler
 */
struct ts_scheduler {
  ts_context *context;
  uint16_t delay_ticks; /**< the scheduler can pause for a number of ticks */
  struct ts_operation *pc; /**< the program counter */
};



inline void ts_delay(struct ts_scheduler *ts, uint16_t ticks) {
  ts->delay_ticks = ticks;
}

/**
 * Change operation sequence by setting the program counter relative
 */
inline void ts_goto(struct ts_scheduler *ts, int offset)
{
  if (ts->pc != NULL) {
    ts->pc += offset;
  }
}

/**
 * End the program
 */
inline void ts_end(struct ts_scheduler *ts)
{
  ts->pc = NULL;
}


/**
 * A scheduler operation doing nothing
 * 
 * Just as a template for your own user defined operations
 */
void ts_op_nop(struct ts_scheduler *ts, void *arguments) {
  (void)ts;
  (void)arguments;
}

/**
 * A scheduler operation printing its string-argument to the serial port
 * 
 * For debugging and as an example for a user operation
 */
void ts_op_prinln(struct ts_scheduler *ts, void *arguments) {
  (void)ts;
  Serial.println((char*)arguments);
}


void ts_op_end(struct ts_scheduler *ts, void *arguments) {
  (void)arguments;
  ts_end(ts);
}


void ts_op_goto(struct ts_scheduler *ts, void *arguments) {
  ts_goto(ts, (int)arguments);
}

void ts_op_delay(struct ts_scheduler *ts, void *arguments) {
  ts_delay(ts, (int)arguments);
}


void ts_exec(struct ts_scheduler *ts, struct ts_operation *program, ts_context *context)
{
  ts->context = context;
  ts->delay_ticks = 0;
  ts->pc = program;
}


void ts_tick(struct ts_scheduler *ts) {
  if (ts == NULL) {
    return;
  }
  if (ts->pc == NULL) {
    return;
  }
  if (ts->delay_ticks > 0) {
    ts->delay_ticks--;
    return;
  }
  if (ts->pc->function == NULL) {
    ts->pc = NULL;
    return;
  }
  ts->pc->function(ts, ts->pc->arguments);
  if (ts->pc != NULL) {
    ts->pc++;
  }
}

#if defined(__cplusplus)
}
#endif

#endif
