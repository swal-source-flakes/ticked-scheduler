/*
  Copyright 2019 Stefan Waldschmidt
    
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

/**
 * @brief Tick-based scheduler used in examples
 *
 * @author Stefan Waldschmidt
 * @copyright Apache License, Version 2.0
 * 
 * https://gitlab.com/swal-source-flakes/ticked-scheduler
 */

#include "Arduino.h"

#include "ticked_scheduler.h"

const uint32_t SERIAL_BAUD = 115200;


void op_led(struct ts_scheduler *ts, void *arguments) {
  int level = (int)arguments;
  digitalWrite(LED_BUILTIN, (level != 0) ? HIGH : LOW);
}


struct ts_scheduler scheduler;

/*
 * Example: Hello World Program
 */

struct ts_operation prog_hello_world[] = {
  {&ts_op_prinln, (void*)"hel "},
  {&ts_op_prinln, (void*)"lo "},
  {&op_led, (void*)1},
  {&ts_op_delay, (void*)200},
  {&op_led, (void*)0},
  {&ts_op_delay, (void*)800},
  {&ts_op_prinln, (void*)"world"},
  {&ts_op_nop, NULL},
  {&ts_op_prinln, (void*)"!"},
  {&ts_op_goto, (void*)-9},
  {&ts_op_end, NULL}
};


void load_program_hello_world()
{
    ts_exec(&scheduler, prog_hello_world, NULL);
}

/*
 *  Example: Faculty calculation
 *  
 *  00         f = 1
 *  01  LOOP:  if (n <= 1) goto END
 *  02         f = f*n
 *  03         n = n-1
 *  04         goto LOOP
 *  05  END:   print f
 */

struct fac_context {
  unsigned int n;
  unsigned int f;
};

void op_set_f_to_1(struct ts_scheduler *ts, void *arguments) {
  struct fac_context *vars = (struct fac_context*)ts->context;
  vars->f = 1;
}

void op_loop_over_n(struct ts_scheduler *ts, void *arguments) {
  struct fac_context *vars = (struct fac_context*)ts->context;
  if (vars->n <= 1) {
    ts_goto(ts, +3);
  }
}

void op_multiply_f(struct ts_scheduler *ts, void *arguments) {
  struct fac_context *vars = (struct fac_context*)ts->context;
  vars->f = vars->f * vars->n;
}

void op_dec_n(struct ts_scheduler *ts, void *arguments) {
  struct fac_context *vars = (struct fac_context*)ts->context;
  vars->n--;
}

void op_print_f(struct ts_scheduler *ts, void *arguments) {
  struct fac_context *vars = (struct fac_context*)ts->context;
  Serial.println(vars->f);
}

struct ts_operation prog_faculty[] = {
  {&op_set_f_to_1, NULL},
  {&op_loop_over_n, NULL},
  {&op_multiply_f, NULL},
  {&op_dec_n, NULL},
  {&ts_op_goto, (void*)-4},
  {&op_print_f, NULL},
  {&ts_op_end, NULL}
};

struct fac_context fac_context;

void load_program_faculty()
{
  fac_context.n = 5;
  ts_exec(&scheduler, prog_faculty, (ts_context*)&fac_context);
}




void setup() {
  Serial.begin(SERIAL_BAUD);
  Serial.println();
  Serial.println();
  Serial.println("setup");

  pinMode(LED_BUILTIN, OUTPUT);

//  load_program_hello_world();
  load_program_faculty();
}


unsigned long last_ms = 0;

void loop() {
  unsigned long now_ms = millis();
  if (now_ms != last_ms) {
    last_ms = now_ms;

    ts_tick(&scheduler);
  }
}
